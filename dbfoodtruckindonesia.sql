-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 01, 2016 at 08:48 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbfoodtruckindonesia`
--

-- --------------------------------------------------------

--
-- Table structure for table `tcareer`
--

CREATE TABLE IF NOT EXISTS `tcareer` (
`CAREERID` int(11) NOT NULL,
  `CAREERFULLNAME` varchar(255) DEFAULT NULL,
  `CAREEREMAIL` varchar(255) DEFAULT NULL,
  `CAREERMOBILE` varchar(255) DEFAULT NULL,
  `CAREERADDRESS` varchar(255) DEFAULT NULL,
  `CAREERCV` varchar(255) DEFAULT NULL,
  `CAREERPHOTO` varchar(255) DEFAULT NULL,
  `CAREERIP` varchar(255) DEFAULT NULL,
  `CAREERDATE` date DEFAULT NULL,
  `CAREERTIME` time DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=55 ;

--
-- Dumping data for table `tcareer`
--

INSERT INTO `tcareer` (`CAREERID`, `CAREERFULLNAME`, `CAREEREMAIL`, `CAREERMOBILE`, `CAREERADDRESS`, `CAREERCV`, `CAREERPHOTO`, `CAREERIP`, `CAREERDATE`, `CAREERTIME`) VALUES
(12, 'Deden Sembada', 'dedenabs@gmail.com', '085755966189', 'Jakarta', 'http://foodtruckindonesia.com/file/cv/Form_Cuti.doc', 'http://foodtruckindonesia.com/file/photo/16_8.jpg', '112.215.66.82', '2014-12-10', '00:09:05'),
(13, 'A. Yazi Widjaya', 'yzwidjaya@yahoo.com', '62-81802212003', 'setiabudhi 191 bandung', 'http://foodtruckindonesia.com/file/cv/CV_2014.nov_.docx', 'http://foodtruckindonesia.com/file/photo/', '118.97.195.234', '2014-12-12', '16:04:37'),
(14, 'MUHAMMAD MUSTHOFA TARIHORAN', 'IMUUUSSS@GMAIL.COM', '08999362228', 'JL.IRIAN JAYA, Kec.PANCORAN MAS DEPOK RAYA', 'http://foodtruckindonesia.com/file/cv/krs-1331010043-1331010043-141214171318.pdf', 'http://foodtruckindonesia.com/file/photo/IMG_0410.JPG', '110.137.239.38', '2014-12-14', '17:20:34'),
(15, 'Mochammad Sabana Iman Akbar', 'banasabana@gmail.com', '0812-9717-0680', 'Jalan Bambu Apus No: 76 Sasak Tinggi Ciputat', 'http://foodtruckindonesia.com/file/cv/CV_SABANA.pdf', 'http://foodtruckindonesia.com/file/photo/Sabana.jpg', '110.137.224.7', '2015-01-18', '09:38:06'),
(16, 'Mochammad Sabana Iman Akbar', 'banasabana@gmail.com', '0812-9717-0680', 'Jalan Bambu Apus No: 76 Sasak Tinggi Ciputat', 'http://foodtruckindonesia.com/file/cv/CV_SABANA1.pdf', 'http://foodtruckindonesia.com/file/photo/Sabana.jpg', '110.137.224.7', '2015-01-18', '09:55:33'),
(17, 'Sabana Iman Akbar', 'banasabana@gmail.com', '081297170680', 'Jalan Bambu Apus No: 76 Sasak Tinggi Ciputat', 'http://foodtruckindonesia.com/file/cv/', 'http://foodtruckindonesia.com/file/photo/', '125.160.104.244', '2015-01-22', '13:42:45'),
(18, '0', '0', '0', '0', 'http://foodtruckindonesia.com/file/cv/', 'http://foodtruckindonesia.com/file/photo/', '61.94.60.183', '2015-01-25', '09:10:22'),
(19, 'Sabana Iman Akbar', 'banasabana@gmail.com', '081297170680', 'Jalan Bambu Apus No: 76 Sasak Tinggi Ciputat', 'http://foodtruckindonesia.com/file/cv/CV_Sabana_Complete.pdf', 'http://foodtruckindonesia.com/file/photo/Sabana.jpg', '61.94.60.183', '2015-01-25', '09:11:50'),
(20, 'Gita Kuswara Dhani', 'gege.dhani@gmail.com', '083840081606', 'Jl Bima Raya RT 01 RW 02 Kmp Mede, Bekasi Jaya, Bekasi Timur, Bekasi', 'http://foodtruckindonesia.com/file/cv/CURRICULUM_VITAE.docx', 'http://foodtruckindonesia.com/file/photo/Foto.jpg', '222.124.57.221', '2015-03-03', '12:15:32'),
(21, '', '', '', '', 'http://foodtruckindonesia.com/file/cv/', 'http://foodtruckindonesia.com/file/photo/', '125.160.206.46', '2015-03-16', '17:00:18'),
(22, '', '', '', '', 'http://foodtruckindonesia.com/file/cv/', 'http://foodtruckindonesia.com/file/photo/', '36.71.50.40', '2015-03-17', '08:46:47'),
(23, 'nancy langi', 'nancilangi@gmail.com', '', '', 'http://foodtruckindonesia.com/file/cv/', 'http://foodtruckindonesia.com/file/photo/', '114.125.171.164', '2015-04-01', '16:20:45'),
(24, 'Rina Wahyuni', 'rh_na93@yahoo.com', '081269341470', 'Jln. Bambu No.66 Medan', 'http://foodtruckindonesia.com/file/cv/RESUME.docx', 'http://foodtruckindonesia.com/file/photo/', '36.76.146.143', '2015-04-12', '22:14:54'),
(25, 'panji gesit aryo seto', 'panjigesit@gmail.com', '081646916888', 'perum johar permai jl.kayu putih blok j no.1 adiarsa, karawang barat, karawang', 'http://foodtruckindonesia.com/file/cv/', 'http://foodtruckindonesia.com/file/photo/IMG_20140610_202604.JPG', '103.246.201.58', '2015-04-14', '15:07:57'),
(26, 'Wakhid Prabowo', 'wakhidprbw@gmail.com', '085695637162', 'bekasi selatan', 'http://foodtruckindonesia.com/file/cv/wakhid_CV.doc', 'http://foodtruckindonesia.com/file/photo/WAKHID100.PNG', '101.255.24.206', '2015-04-15', '12:17:07'),
(27, 'Wakhid Prabowo', 'wakhidprbw@gmail.com', '085695637162', 'bekasi selatan', 'http://foodtruckindonesia.com/file/cv/wakhid_CV1.doc', 'http://foodtruckindonesia.com/file/photo/WAKHID1001.PNG', '101.255.24.206', '2015-04-15', '12:17:26'),
(28, '', '', '', '', 'http://foodtruckindonesia.com/file/cv/', 'http://foodtruckindonesia.com/file/photo/', '128.199.207.123', '2015-04-15', '15:31:15'),
(29, '', '', '', '', 'http://foodtruckindonesia.com/file/cv/', 'http://foodtruckindonesia.com/file/photo/', '128.199.207.123', '2015-04-15', '15:31:17'),
(30, '', '', '', '', 'http://foodtruckindonesia.com/file/cv/', 'http://foodtruckindonesia.com/file/photo/', '128.199.207.123', '2015-04-15', '15:31:17'),
(31, '', '', '', '', 'http://foodtruckindonesia.com/file/cv/', 'http://foodtruckindonesia.com/file/photo/', '128.199.207.123', '2015-04-15', '15:31:17'),
(32, '', '', '', '', 'http://foodtruckindonesia.com/file/cv/', 'http://foodtruckindonesia.com/file/photo/', '128.199.207.123', '2015-04-15', '15:31:17'),
(33, '', '', '', '', 'http://foodtruckindonesia.com/file/cv/', 'http://foodtruckindonesia.com/file/photo/', '128.199.207.123', '2015-04-15', '15:31:17'),
(34, '', '', '', '', 'http://foodtruckindonesia.com/file/cv/', 'http://foodtruckindonesia.com/file/photo/', '128.199.207.123', '2015-04-15', '15:31:17'),
(35, '', '', '', '', 'http://foodtruckindonesia.com/file/cv/', 'http://foodtruckindonesia.com/file/photo/', '128.199.207.123', '2015-04-15', '15:31:17'),
(36, '', '', '', '', 'http://foodtruckindonesia.com/file/cv/', 'http://foodtruckindonesia.com/file/photo/', '128.199.207.123', '2015-04-15', '15:31:17'),
(37, '', '', '', '', 'http://foodtruckindonesia.com/file/cv/', 'http://foodtruckindonesia.com/file/photo/', '128.199.207.123', '2015-04-15', '15:31:17'),
(38, '', '', '', '', 'http://foodtruckindonesia.com/file/cv/', 'http://foodtruckindonesia.com/file/photo/', '128.199.207.123', '2015-04-15', '15:31:17'),
(39, '', '', '', '', 'http://foodtruckindonesia.com/file/cv/', 'http://foodtruckindonesia.com/file/photo/', '128.199.207.123', '2015-04-15', '15:31:18'),
(40, '', '', '', '', 'http://foodtruckindonesia.com/file/cv/', 'http://foodtruckindonesia.com/file/photo/', '128.199.207.123', '2015-04-15', '15:31:32'),
(41, '', '', '', '', 'http://foodtruckindonesia.com/file/cv/', 'http://foodtruckindonesia.com/file/photo/', '128.199.207.123', '2015-04-15', '15:31:32'),
(42, '', '', '', '', 'http://foodtruckindonesia.com/file/cv/', 'http://foodtruckindonesia.com/file/photo/', '128.199.207.123', '2015-04-15', '15:31:32'),
(43, '', '', '', '', 'http://foodtruckindonesia.com/file/cv/', 'http://foodtruckindonesia.com/file/photo/', '128.199.207.123', '2015-04-15', '15:31:32'),
(44, '', '', '', '', 'http://foodtruckindonesia.com/file/cv/', 'http://foodtruckindonesia.com/file/photo/', '128.199.207.123', '2015-04-15', '15:31:32'),
(45, 'Meiza Aulia Yohana', 'meizaauliayohana1@gmail.com', '087872081169', 'Perum, Bojong Depok Baru 2 Blok BN 15, Cibinong-Kab. Bogor', 'http://foodtruckindonesia.com/file/cv/CV_MEIZA_kerudung.pdf', 'http://foodtruckindonesia.com/file/photo/kerudung.jpg', '128.199.207.123', '2015-04-15', '15:36:10'),
(46, 'Meiza Aulia Yohana', 'meizaauliayohana1@gmail.com', '087872081169', 'Perum, Bojong Depok Baru 2 Blok BN 15, Cibinong-Kab. Bogor', 'http://foodtruckindonesia.com/file/cv/', 'http://foodtruckindonesia.com/file/photo/kerudung.jpg', '128.199.207.123', '2015-04-15', '15:46:12'),
(47, 'Meiza Aulia Yohana', '0', '0', 'Perum, Bojong Depok Baru 2 Blok BN 15, Cibinong-Kab. Bogor', 'http://foodtruckindonesia.com/file/cv/', 'http://foodtruckindonesia.com/file/photo/', '128.199.207.123', '2015-04-15', '15:46:46'),
(48, 'Meiza Aulia Yohana', 'meizaauliayohana1@gmail.com', '087872081169', 'Perum, Bojong Depok Baru 2 Blok BN 15, Cibinong-Kab. Bogor', 'http://foodtruckindonesia.com/file/cv/CV_MEIZA_kerudung1.pdf', 'http://foodtruckindonesia.com/file/photo/kerudung.jpg', '128.199.207.123', '2015-04-15', '15:47:35'),
(49, '0', '0', '0', '0', 'http://foodtruckindonesia.com/file/cv/', 'http://foodtruckindonesia.com/file/photo/', '185.53.44.64', '2015-05-02', '11:42:18'),
(50, '0', '0', '0', '0', 'http://foodtruckindonesia.com/file/cv/', 'http://foodtruckindonesia.com/file/photo/', '69.84.207.246', '2015-05-15', '12:54:09'),
(51, 'Hendrik surjono', 'Intaf889@yahoo.co.id', '081339995858', 'Emping malang 25a', 'http://foodtruckindonesia.com/file/cv/', 'http://foodtruckindonesia.com/file/photo/', '36.85.34.101', '2015-05-17', '22:49:07'),
(52, 'aris diyanto', 'is.ar92@yahoo.com', '08994071381', 'Jl Cipinang Asem rt.12/04 Kel. Kebon Pala Kec.Makasar Jaktim', 'http://foodtruckindonesia.com/file/cv/CV_Arisdiyanto.docx', 'http://foodtruckindonesia.com/file/photo/Foto_aris.jpg', '57.73.28.4', '2015-05-27', '11:44:55'),
(53, 'yuan pramayuda', 'yuanpramayuda@gmail.com', '087722674025', 'jl rumah sakit no 11 tasikmalaya', 'http://foodtruckindonesia.com/file/cv/CV_Yuan_Pramayuda.docx', 'http://foodtruckindonesia.com/file/photo/', '112.215.66.77', '2015-05-28', '11:43:36'),
(54, 'yuan pramayuda', 'yuanpramayuda@gmail.com', '087722674025', 'jl rumah sakit no 11 tasikmalaya', 'http://foodtruckindonesia.com/file/cv/CV_Yuan_Pramayuda1.docx', 'http://foodtruckindonesia.com/file/photo/', '112.215.66.69', '2015-05-28', '11:45:50');

-- --------------------------------------------------------

--
-- Table structure for table `tcontact`
--

CREATE TABLE IF NOT EXISTS `tcontact` (
`CONTACTID` int(11) NOT NULL,
  `CONTACTNAME` varchar(255) DEFAULT NULL,
  `CONTACTEMAIL` varchar(255) DEFAULT NULL,
  `CONTACTMESSAGE` text,
  `CONTACTIP` varchar(255) DEFAULT NULL,
  `CONTACTDATE` date DEFAULT NULL,
  `CONTACTTIME` time DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=82 ;

--
-- Dumping data for table `tcontact`
--

INSERT INTO `tcontact` (`CONTACTID`, `CONTACTNAME`, `CONTACTEMAIL`, `CONTACTMESSAGE`, `CONTACTIP`, `CONTACTDATE`, `CONTACTTIME`) VALUES
(1, '', '', '', '202.155.149.253', '2014-12-09', '02:21:35'),
(2, 'Deden Sembada', 'dedenabs@gmail.com', 'Tes Message', '202.155.149.253', '2014-12-09', '02:22:25'),
(3, 'Deden Sembada', 'dedenabs@gmail.com', 'Tes  asssssssssssssssssssa', '202.155.149.253', '2014-12-09', '02:23:38'),
(4, '0', '0', '0', '23.232.154.233', '2014-12-09', '15:36:54'),
(5, 'Welma Paliama', 'welma@lottemart.co.id', 'Dear Food Truck Indonesia,', '202.137.14.133', '2014-12-22', '15:36:24'),
(6, 'Welma Paliama', 'welma@lottemart.co.id', 'Dear Food Truck Indonesia, apakah bisa diberikan kesempatan kepada Lottemart Wholesale (Ex Makro) untuk bisa menjadi dealer penjualan Food Truck di Indonesia. Saat ini Lottemart Wholesale ada 24 toko yang tersebar di Jabodetabek, Jawa, Sumatera, Sulawesi dan Kalimantan.', '202.137.14.133', '2014-12-22', '15:38:34'),
(7, 'krist', 'kris@gusnoproduction.com', 'Kami dari Gusno Production menawarkan utk bergabung dlm acara grand opening sentra otomotif AXC summarecon bekasi pd tanggal 31 Jan-01 Feb 2015,hanya dgn barter produk 15 porsi perhari sdh dapat ikut bergabung utk memeriahkan event grand opening AXC Summarecon,for more info please contact 021-73883874,terima kasih ', '110.137.237.30', '2015-01-13', '15:21:46'),
(8, 'Meridiana Kristine', 'meridiana.mas@gmail.com', 'Selamat sore, saya dari mall @ alam sutera mau mengundang Britatoes untuk berpartisipasi dalam Food Truck Parade di mall @ alam sutera yang akan diselenggarakan pada tanggal 12 Feb - 15 Feb mendatang. Untuk info dan pembicaraan lebih lanjut ditunggu balasan ke email yang saya tinggalkan. terima kasih banyak.', '114.6.20.38', '2015-01-13', '15:25:26'),
(9, 'Otto nasri', 'otto.nasri@yahoo.com', 'Saya tertarik dengan food truck', '111.94.194.37', '2015-01-23', '18:57:07'),
(10, 'Maya Yenindra', 'mayday.bb2@gmail.com', 'how to start business wit food track ? what kind of the prosedure ? Tq', '118.100.121.208', '2015-01-23', '19:32:30'),
(11, 'Maya Yenindra', 'mayday.bb2@gmail.com', 'how to start business wit food track ? what kind of the prosedure ? Tq', '118.100.121.208', '2015-01-23', '19:32:49'),
(12, 'Eka', 'ekayen@aol.com', 'Is this food truck association, I love to cook and love to do food truck., I would like to know more about food truck industry and  its opportunity', '116.0.6.154', '2015-01-24', '15:27:38'),
(13, 'Eka', 'ekayen@aol.com', 'Is this food truck association, I love to cook and love to do food truck., I would like to know more about food truck industry and  its opportunity', '116.0.6.154', '2015-01-24', '15:27:42'),
(14, 'Eka', 'ekayen@aol.com', 'Is this food truck association, I love to cook and love to do food truck., I would like to know more about food truck industry and  its opportunity', '116.0.6.154', '2015-01-24', '15:27:44'),
(15, '', '', '', '182.253.33.246', '2015-01-26', '16:49:53'),
(16, 'Fikry Ariandy', 'dunksky25@gmail.com', 'Saya mau tanya, food truck indonesia ini perkumpulan atau organisasi food truckatau bukan ?', '139.193.106.66', '2015-01-28', '22:53:21'),
(17, 'Roy Fadly', 'roy.fadly@gmail.com', 'Hi, where will Britatoes be at 25th - 27th March? If avail, are you interested with our bazaar? Thanks', '125.161.148.250', '2015-01-29', '22:01:49'),
(18, 'Adrian Zmith ', 'zmith_adrian@me.com', 'Hi Foodtruck Mandiri Prakasa, how do we find a person to contact for proposing a partnership. we would like to have a foodtruck with café concept, concerning quality of coffee (we roast the beans) and variety of food. Regards, Zmith', '139.255.36.147', '2015-02-01', '17:05:30'),
(19, 'inawaty', 'tan_ie_mong@yahoo.co.id', 'ingin join komunitas foodtruck indonesia', '112.215.66.77', '2015-02-16', '13:16:53'),
(20, 'inawaty', 'tan_ie_mong@yahoo.co.id', 'ingin join komunitas foodtruck indonesia', '112.215.66.69', '2015-02-16', '13:17:14'),
(21, 'Sonata matra', 'sonatamatraa@gmail.com', 'hallo saya ingin mengajak jakarta foodtruck,bergabung ke acara kita di pantai indah kapuk (PIK) pada tanggal 6 7  8 boleh minta no telp yang bisa di hubungi buat kita hubungi..thank you ', '36.77.222.122', '2015-02-16', '13:51:28'),
(22, 'hendrik hendriyanto', 'chatime.mobile001@FBINDO.COM', 'Haii admin', '203.161.24.195', '2015-02-16', '21:17:43'),
(23, 'Pusat Info Bazaar Jakarta', 'pelakubazaar@gmail.com', 'Hai, Salam kenal yaa', '180.214.233.23', '2015-02-19', '01:38:46'),
(24, 'Laura', 'laurapreciousbless@gmail.com', 'Bisa catering ya? Gimana caranya ya klo mau catering?', '139.228.57.136', '2015-02-28', '18:25:06'),
(25, 'Monique Alamsjah', 'malamsjah@8blackstones.com', 'Hi! I''m from MARIKAKA (www.marikaka.com). I was wondering if you''re in the market for an online Point-of-Sale that''s fully integrated with retail management solution. You can subscribe monthly or even rent our POS system with iPad mini. I can be contacted at 081316925145. Looking forward to hearing from you. ', '180.244.8.25', '2015-03-03', '17:59:03'),
(26, 'Hery', 'hery.liandro@gmail.com', 'Hi, mau tanya konsep yang ditawarkan foodtruck itu masakan apa? Tertarik untuk buka cabang/franchise?', '139.193.60.39', '2015-03-04', '17:39:27'),
(27, '0', '0', '0', '120.198.243.130', '2015-03-05', '04:43:04'),
(28, 'Rezki Ramadhani', 'rezki.ramadhani@gmail.com', 'Dear Foodtruck Indonesia,', '112.215.66.68', '2015-03-12', '17:13:54'),
(29, 'Rezki Ramadhani', 'rezki.ramadhani@gmail.com', 'Halo, saya sedang merencanakan untuk membuat foodtruck dalam tahun ini. Oleh karena itu, saya ingin bergabung dalam komunitas foodtruck di Jakarta. Saya menemukan website ini, walaupun saya ragu apakah ini website untuk komunitas foodtruck, atau milik Britatoes saja. Mohon pencerahannya. Saya berharap dapat menjalin komunikasi dengan Britatoes maupun komunitas foodtruck Indonesia. Terima kasih.', '112.215.66.74', '2015-03-12', '17:19:53'),
(30, 'reynon', 'reynon.vigor@gmail.com', 'Dear Food Truck', '202.151.10.50', '2015-03-24', '11:57:23'),
(31, 'reynon', 'reynon.vigor@gmail.com', 'Dear Food Truck, need info if we want to make event with food truck as a part of the event.please let me know', '202.151.10.50', '2015-03-24', '11:58:55'),
(32, 'Laura', 'Laurapreciousbless.khoe@gmail.com', 'Kalo mau catering gimana caranya ya?', '180.251.208.22', '2015-03-27', '14:42:41'),
(33, 'Jason Lynn', 'chef-jason@hotmail.com', 'Hi i am an Executive Chef from New Zealand who is Married to an Indonesian. I am looking at ways that i can live & work in Indonesia.', '37.131.71.42', '2015-03-29', '17:31:12'),
(34, 'Jason Lynn', 'chef-jason@hotmail.com', 'Please share information about your Company & Concept.', '37.131.71.42', '2015-03-29', '17:37:06'),
(35, '', '', '', '114.121.160.24', '2015-03-31', '01:33:26'),
(36, 'nancy langi', 'nancilangi@gmail.com', 'what kind facility do u offer ', '114.125.171.164', '2015-04-01', '16:07:43'),
(37, 'nancy langi', 'nancilangi@gmail.com', 'what kind facility do u offer ', '114.125.171.164', '2015-04-01', '16:07:49'),
(38, '0', '0', '0', '114.125.171.164', '2015-04-01', '16:21:16'),
(39, 'fadlyansyah', 'm.a.fadlyansyah@gmail.com', 'saya mau bertanya bagaimana cara franchise merk anda?', '180.251.169.56', '2015-04-03', '14:28:46'),
(40, 'iwanfachrian', 'iwanfachrian@gmail.com', 'please send your offering food truck indonesia tks ', '36.83.49.179', '2015-04-10', '07:29:14'),
(41, 'Novie', 'novie.nurlisyah@gmail.com', 'Hai guys,,,aku pengen banget punya bisnis food truck seperti ini,kalo mau joint for invest gimana caranya yaa,,,bisa atau nggak ?', '39.252.40.249', '2015-04-10', '16:34:06'),
(42, '0', '0', '0', '103.246.38.196', '2015-04-11', '13:40:48'),
(43, 'Iwan lukito', 'sayalukito@gmail.com', 'Halo selamat siang, ini bisa bangun foodtruck? harga berapa ya untuk foodtruck seperti britatoes punya. kelistrikan gmna? gas gmna? secure gk?', '125.161.24.171', '2015-04-11', '13:40:48'),
(44, 'Reza Fahlevi', 'cheffreza@gmail.com', 'Dear Sir,', '78.100.186.49', '2015-04-12', '11:45:54'),
(45, 'Reza Fahlevi', 'cheffreza@gmail.com', 'Dear Sir, I would like to know more about the food truck, I want to have it for my own business, please let me know how I can have it .', '78.100.186.49', '2015-04-12', '11:48:44'),
(46, 'Reza Fahlevi', 'cheffreza@gmail.com', 'Dear Sir, I would like to know more about the food truck, I want to have it for my own business, please let me know how I can have it .', '78.100.186.49', '2015-04-12', '11:48:45'),
(47, 'Reza Fahlevi', 'cheffreza@gmail.com', 'Dear Sir, I would like to know more about the food truck, I want to have it for my own business, please let me know how I can have it . Thanks and Regards, Reza Fahlevi', '78.100.186.49', '2015-04-12', '11:49:38'),
(48, 'Reza Fahlevi', 'cheffreza@gmail.com', 'Dear Sir, I would like to know more about the food truck, I want to have it for my own business, please let me know how I can have it . Thanks and Regards, Reza Fahlevi', '78.100.186.49', '2015-04-12', '17:41:02'),
(49, 'Isnaini Emas Barwi', 'isnainieb@yahoo.com', 'hallo kak, mau tanya nih. Kalo mau  bekerja sama dengan komunitas food truck indonesia gimana caranya? kita mau bikin event nih', '103.23.103.114', '2015-04-20', '13:00:01'),
(50, 'dewi rosanah', 'dedeww88@gmail.com', '  Kami dari Keluarga Besar HMRC Chapter BODECI ingin mengajak dan mengundang Food Truck  Indonesia,Untuk meramaikan acara kami dalam rangka "4th anniversary HMRC Chapter BODECI dan Menggalakan Kopdar Bersama 3 Bulanan HMRC Tahun 2015" yang akan kami laksanakan:  Hari : Minggu Tanggal : 17 Mei 2015 Tempat : GOR Cibinong Kabupaten Bogor  Untuk info lebih lengkapnya, kami juga menyertakan proposal sebagai bahan pertimbangan. Terima kasih.           Hormat Kami,         Dewi Rosanah HMRC 1415         Honda Maesto Rider Club Chapter Bodeci.     Untuk nomer yang bisa di hubungin: 1. Fino Machtoeb (Ketua Panitia) : 08111992355 2. Didiet Prasetyo (Koordinator) : 082114139001 3. Dewi Rosanah (Bendahara) : 087882416965', '110.138.113.46', '2015-04-21', '11:44:31'),
(51, 'Olaf Christmantoro', 'olafchrist03@gmail.com', 'apakah food truck ada komunitas nya? kami berminat untuk mengadakan culinary night bersama food truck.. mohon konfirmasinya..', '36.72.178.60', '2015-04-24', '18:54:06'),
(52, 'jessica', 'jessicajordanius@gmail.com', 'ingin tahu info utk memulai usaha ini,butuh modal brp.thanks', '202.67.41.51', '2015-04-24', '22:44:34'),
(53, 'diany nuriany', 'aqsa_boy@yahoo.co.id', 'saya sangat berminat mempunyai usaha food truck,tp apa bs dgn modal yg pas2an saya bs..?tolong dibantu kira2 ada link yg bs bantu agar saya pnya usaha ini..thx', '103.230.48.235', '2015-04-26', '23:05:35'),
(54, '0', '0', '0', '185.53.44.203', '2015-04-28', '21:03:07'),
(55, 'James SabbatH', 'jsabbath.7079@gmail.com', 'We are a residential developer at Jakarta Barat. I want to invite Food Trucks Indonesia to come to our Marketing Gallery join our event at middle of this May.  Thank you', '180.252.29.211', '2015-05-04', '10:49:32'),
(56, 'hapindi', 'mhapindiadp@yahoo.com', 'selamat pagi, saya mau nanya nih kalau mau medatangkan food truck ke kampus bagaimana ya alurnya? mohon info, thanx :)', '180.250.133.82', '2015-05-11', '06:38:52'),
(57, 'Rillo Maulana', 'rillomaulana@gmail.com', 'kami dari mahasiswa Teknik Sipil STT-PLN, akan menyelenggarakan bazaar foodtruck di tanggal 23 Mei 2015, apakah britatoes bisa?? jika iya, kami tunggu untuk MoUnya', '120.169.255.6', '2015-05-14', '09:29:13'),
(58, 'Rillo Maulana', 'rillomaulana@gmail.com', 'kami dari mahasiswa Teknik Sipil STT-PLN, akan menyelenggarakan bazaar foodtruck di tanggal 23 Mei 2015, apakah britatoes bisa?? jika iya, kami tunggu untuk MoUnya', '120.169.255.6', '2015-05-14', '09:29:34'),
(59, 'budiman', 'my.alvaro09@gmail.com', 'saya mau foodtruck bagaimana caranya ? unt daerah jatim', '125.164.156.111', '2015-05-15', '09:09:02'),
(60, 'budiman', 'my.alvaro09@gmail.com', 'saya mau foodtruck bagaimana caranya ? unt daerah jatim', '125.164.156.111', '2015-05-15', '09:09:08'),
(61, '0', '0', '0', '69.84.207.246', '2015-05-15', '12:54:10'),
(62, 'Ernest', 'ernestrumbang1@yahoo.co.id', '', '36.78.97.240', '2015-05-16', '21:04:02'),
(63, 'Hendrik', 'intaf889@yahoo.co.id', 'Saya mau bergabung menjadi member food truck', '36.85.34.101', '2015-05-17', '22:48:12'),
(64, 'very', 'very_oow@yahoo.com', 'Dear ,', '114.79.48.237', '2015-05-20', '22:41:46'),
(65, 'very', 'very_oow@yahoo.com', 'Dear,   Sy dengan very, usia 32. background di hospitality F & B khususnya di Cruise Ship hampir selama 10 thn.  sy bermaksud untuk menawarkan kerjasama yang dimana sy sudah lama berkeingiinan untuk memulai usaha F & B di Food Truck, sesuai dengan skill sy. Bila ada waktunya saya bisa untuk menjelaskan lebih lanjut tentang proposal saya lebih jelas dengan pertemuan. mohon di info nya.  many thanks Very Best Regards,', '114.79.48.237', '2015-05-20', '22:47:47'),
(66, 'Zuli Marlina', 'bellezuli@yahoo.com', 'Vriends Organizer akan mengadakan bazaar & garage sale charity (amal) di mana seluruh hasil penjualan panitia (hanya yg di terima panitia) akan disumbangkan ke panti asuhan dan pembangunan sekolah yg terhambat. Bazaar & garage sale akan diadakan pada ; Hari & tgl : sabtu 30 mei 2015 jam 9.00 wib - 21.00 wib dan minggu 31 mei 2015 jam 9.00 wib- 16.00 wib Lokasi : Lapangan SDN 11-12 Kalibata Tengah Jakarta. Dengan 40 stand makanan/minuman, pakaian, asesoris, kosmetik dan mainan hingga otomotif serta perumahan, lokasi yang aman, nyaman serta parkir yang memadai. Kesempatan baik bagi anda yang ingin membuka stand untuk mempromosikan dagangannya. Hanya Rp 250.000/2 hari. Daftarkan stand anda di 081315530539 (Zuli).Tersisa 15 stand lagi... Yuk buruan daftar :D', '139.193.112.109', '2015-05-21', '15:47:45'),
(67, 'christine', 'christineyunilife@yahoo.com', 'hi :) saya mau tanya apa ini franchise yg sudah bisa dibeli atau just for personal. im interesting to buy soalnya. i need info please. thankyou.', '103.10.65.124', '2015-05-23', '11:34:44'),
(68, 'Stephanie', 'Stephanie.benita@hotmail.com', 'Hi there, my name is Stephanie and i found your website as I was browsing the internet for food truck movement in Indonesia. In your page I found the career section and I was just wondering what kind of career opportunity do you provide. Would love to hear back from you. Thanks!', '60.241.117.83', '2015-05-26', '17:01:45'),
(69, 'cevin willmart', 'cevinwillmart@gmail.com', 'ini di indonesia ?', '36.85.43.131', '2015-05-27', '18:54:34'),
(70, 'cevin willmart', 'cevinwillmart@gmail.com', 'ini di indonesia ?', '36.85.43.131', '2015-05-27', '18:54:56'),
(71, 'Wahid Maulana', 'wahidmaulana@yahoo.co.id', 'Dear Food Truck Indonesia Ditempat  Perkenankan kami dari PT. Astra International, Tbk - Isuzu Cab. Muara Karang, Jakarta Utara. Ingin memberikan penawaran harga kendaraan mobil dari Isuzu. Adapun untuk penawaran kendaraan mobil Isuzu tsb kami lampirkan didalam file, Atas waktu dan perhatian yang telah diberikan kami mengucapkan terima kasih.   Best regards,  Wahid Maulana PT. Astra International, Tbk - Isuzu Jl. Pluit Karang Timur Blok B 8 No. 76-77 Muara Karang - Jakarta Utara Hp        : 021 - 3590 5778 GSM     : 0812 8374 6545', '103.28.188.15', '2015-05-28', '09:47:23'),
(72, 'Zulkifli', 'zulkifliunhas@gmail.com', 'Hi! Saya mau tanya jumlah pengusaha food truck di Indonesia sekarang ada berapa yah (estimasi juga boleh)?', '112.215.69.106', '2015-06-01', '08:42:26'),
(73, 'monika', 'nelly.monika@ymail.com', 'selamat siang, saya ingin tanya harga kisaran food truck dan apa saja yang dapat untuk harga yang ditawarkan. saya berminat untuk memjual makanan menggunakan food truck, terimakasih', '126.205.27.240', '2015-06-04', '12:12:09'),
(74, 'Ahmad Romero Comacho', 'ahmad.romero2412@gmail.com', 'Dear Foodtruckindonesia,', '64.233.173.169', '2015-06-04', '15:46:20'),
(75, 'Ahmad Romero Comacho', 'ahmad.romero2412@gmail.com', 'Dear Foodtruckindonesia, mohon info contact person terkait proposal kerjasama event. terima kasih.', '64.233.173.164', '2015-06-04', '15:46:51'),
(76, 'erine marcelina', 'erine.marcellina@ymail.com', 'hello, saya mau tanya-tanya seputar foodtruck boleh? krn saya panitia bazaar dan mau mengundang foodtruck tp ini pengalaman pertama kami. jd mau tanya-tanya kisaran harga foodtruck kalau join foodfestival. trims', '36.70.105.73', '2015-06-06', '20:30:17'),
(77, 'Frenly', 'frenly_kumala@yahoo.com', 'I want to know how to have a franchise partnership with britatoes, i''m from medan, north sumatra', '202.62.16.21', '2015-06-08', '16:18:02'),
(78, 'Kenneth', 'aaronkenneth2@gmail.com', 'Kalau mau kirim proposal event ke mana ya? Emailnya? ', '36.77.249.234', '2015-06-11', '10:33:13'),
(79, 'sylvester william', 'sylvesterwilliam99@yahoo.com', 'jika saya tertarik untuk bergabung di bisnis ini,berapa banyak investasi yang perlu saya keluarkan untuk mendapatkan brand dari foodtruck indonesia?', '180.249.154.241', '2015-06-19', '11:21:18'),
(80, 'sylvester william', 'sylvesterwilliam99@yahoo.com', 'jika saya tertarik untuk bergabung di bisnis ini,berapa banyak investasi yang perlu saya keluarkan untuk mendapatkan brand dari foodtruck indonesia?', '180.249.154.241', '2015-06-19', '11:21:28'),
(81, '', '', '', '::1', '2015-06-20', '00:43:31');

-- --------------------------------------------------------

--
-- Table structure for table `tgallery`
--

CREATE TABLE IF NOT EXISTS `tgallery` (
`GALLERYID` int(11) NOT NULL,
  `GALLERYTITLE` varchar(255) DEFAULT NULL,
  `GALLERYPATH` varchar(255) DEFAULT NULL,
  `UPLOADDATE` date DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `tgallery`
--

INSERT INTO `tgallery` (`GALLERYID`, `GALLERYTITLE`, `GALLERYPATH`, `UPLOADDATE`) VALUES
(1, 'Picture1', 'Picture1.jpg', '2014-11-30'),
(2, 'Picture2', 'Picture2.jpg', '2014-11-30'),
(3, 'Picture3', 'Picture3.jpg', '2014-11-30'),
(4, 'Picture4', 'Picture4.jpg', '2014-11-30'),
(5, 'Picture5', 'Picture5.jpg', '2014-11-30'),
(6, 'Picture6', 'Picture6.jpg', '2014-11-30'),
(7, 'IMG_9774', 'IMG_9774.JPG', '2014-11-30'),
(8, 'IMG_9775', 'IMG_9775.PNG', '2014-11-30'),
(9, 'IMG_9778', 'IMG_9778.JPG', '2014-11-30'),
(10, 'IMG_9877', 'IMG_9877.JPG', '2014-11-30'),
(11, 'IMG_9780', 'IMG_9780.JPG', '2014-11-30'),
(12, 'MeNPotatoes', 'MeNPotatoes.jpg', '2014-11-30'),
(13, 'IMG_9777', 'IMG_9777.JPG', '2014-11-30'),
(14, 'IMG_1229', 'IMG_1229.JPG', '2014-11-30'),
(15, 'IMG_1230', 'IMG_1230.JPG', '2014-11-30'),
(16, '1375847_351497375021939_5149773005080921500_n', '1375847_351497375021939_5149773005080921500_n.jpg', '2015-01-06'),
(17, '10418917_355442144627462_3495373875514731347_n', '10418917_355442144627462_3495373875514731347_n.jpg', '2015-01-06'),
(18, 'Attachment-1', 'Attachment-1.jpeg', '2015-01-06'),
(19, 'IMG_0194', 'IMG_0194.JPG', '2015-01-06'),
(20, 'IMG_0195', 'IMG_0195.JPG', '2015-01-06'),
(21, 'IMG_0196', 'IMG_0196.JPG', '2015-01-06'),
(22, 'IMG_0197', 'IMG_0197.JPG', '2015-01-06'),
(23, 'IMG_9702', 'IMG_9702.JPG', '2015-01-06'),
(24, 'IMG_9708', 'IMG_9708.JPG', '2015-01-06'),
(25, 'IMG_9709', 'IMG_9709.JPG', '2015-01-06'),
(26, 'IMG_9734', 'IMG_9734.JPG', '2015-01-06'),
(27, 'IMG_9875', 'IMG_9875.JPG', '2015-01-06'),
(28, 'IMG_9876', 'IMG_9876.JPG', '2015-01-06'),
(29, 'IMG_9877', 'IMG_9877.JPG', '2015-01-06'),
(30, 'IMG_9913', 'IMG_9913.JPG', '2015-01-06'),
(31, 'photo 1', 'photo 1.JPG', '2015-01-15'),
(32, 'photo 2', 'photo 2.JPG', '2015-01-15'),
(33, 'photo 3', 'photo 3.JPG', '2015-01-15'),
(34, 'photo 4', 'photo 4.JPG', '2015-01-15'),
(35, 'photo 5', 'photo 5.JPG', '2015-01-15'),
(36, 'photo.JPG', 'photo.JPG', '2015-01-22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tcareer`
--
ALTER TABLE `tcareer`
 ADD PRIMARY KEY (`CAREERID`);

--
-- Indexes for table `tcontact`
--
ALTER TABLE `tcontact`
 ADD PRIMARY KEY (`CONTACTID`);

--
-- Indexes for table `tgallery`
--
ALTER TABLE `tgallery`
 ADD PRIMARY KEY (`GALLERYID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tcareer`
--
ALTER TABLE `tcareer`
MODIFY `CAREERID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `tcontact`
--
ALTER TABLE `tcontact`
MODIFY `CONTACTID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT for table `tgallery`
--
ALTER TABLE `tgallery`
MODIFY `GALLERYID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
