<?php
class Mcareer extends CI_Model{
	//add new tracking banner
	private $tcareer = 'TCAREER';
	
	function Career(){
		parent::Model();
	}
	
	
	function submitCareer($career){
		$this->db->insert($this->tcareer, $career);
		return $this->db->insert_id();
	}
}

/*
	CREATE TABLE TCAREER (
		CAREERID INT NOT NULL AUTO_INCREMENT,
		CAREERFULLNAME VARCHAR(255),
		CAREEREMAIL VARCHAR(255),
		CAREERMOBILE VARCHAR(255),
		CAREERADDRESS VARCHAR(255),
		CAREERCV VARCHAR(255),
		CAREERPHOTO VARCHAR(255),
		CAREERIP VARCHAR(255),
		CAREERDATE DATE,
		CAREERTIME TIME,
		PRIMARY KEY (CAREERID)
	)
*/
?>