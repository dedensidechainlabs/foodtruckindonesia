<?php
class Mdata extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }
 
	public function count_band() {
		$this->db->where('TYPE', 1);
		$this->db->from('TDATA');
		return $this->db->count_all_results();  
	}
	
    public function fetch_tband($limit, $start) {
        //$this->db->select('tfoto.*,tregistrasi.*');
		$this->db->limit($limit, $start);
        $this->db->from('TDATA');
		$this->db->where('TYPE',1);
		//$this->db->join('tregistrasi', 'tfoto.REGISTRASIID = tregistrasi.REGISTRASIID', 'left'); 
		//$this->db->order_by("UPLOADDATE", "DESC"); 
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
    
	public function count_player() {
		$this->db->where('TYPE', 2);
		$this->db->from('TDATA');
		return $this->db->count_all_results();  
	}
	
    public function fetch_tplayer($limit, $start) {
        //$this->db->select('tfoto.*,tregistrasi.*');
		$this->db->limit($limit, $start);
        $this->db->from('TDATA');
		$this->db->where('TYPE',2);
		//$this->db->join('tregistrasi', 'tfoto.REGISTRASIID = tregistrasi.REGISTRASIID', 'left'); 
		//$this->db->order_by("UPLOADDATE", "DESC"); 
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
   
	public function count_event() {
		$this->db->from('TEVENT');
		return $this->db->count_all_results();  
	}
	
	public function fetch_tevent($limit, $start) {
        //$this->db->select('TEVENT.*,TDATA.*');
		$this->db->limit($limit, $start);
        $this->db->from('TEVENT');
		//$this->db->where('TYPE',2);
		$this->db->join('TDATA', 'TEVENT.DATAID = TDATA.DATAID', 'left'); 
		$this->db->order_by("EVENTDATE", "DESC"); 
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
}
?>