<?php
class Mcontact extends CI_Model{
	//add new tracking banner
	private $tcontact = 'TCONTACT';
	
	function Contact(){
		parent::Model();
	}
	
	
	function submitContact($contact){
		$this->db->insert($this->tcontact, $contact);
		return $this->db->insert_id();
	}
}

/*
	CREATE TABLE TCONTACT (
		CONTACTID INT NOT NULL AUTO_INCREMENT,
		CONTACTNAME VARCHAR(255),
		CONTACTEMAIL VARCHAR(255),
		CONTACTMESSAGE TEXT,
		CONTACTIP VARCHAR(255),
		CONTACTDATE DATE,
		CONTACTTIME TIME,
		PRIMARY KEY (CONTACTID)
	)
*/
?>