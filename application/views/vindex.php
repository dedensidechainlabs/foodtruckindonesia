<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo (isset($description)?$description:"");?>">
    <meta name="author" content="<?php echo (isset($author)?$author:""); ?>">
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/image/logo.png">
    <title><?php echo (isset($title)?$title:"");?></title>
	
	<link href='http://fonts.googleapis.com/css?family=Cabin+Condensed' rel='stylesheet' type='text/css'>
	<link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
	<script src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
	<script>
		$(function() {
		  $('a[href*=#]:not([href=#])').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			  var target = $(this.hash);
			  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			  if (target.length) {
				$('html,body').animate({
				  scrollTop: target.offset().top
				}, 1000);
				return false;
			  }
			}
		  });
		});
	</script>
  </head>

  <body>
	<div class="container-fluid">
	<div class="container" >
	  <div class="masthead">
        <br>
        <br>
        <ul class="nav nav-justified">
			<li><a href="#home">HOME</a></li>
			<li><a href="#about">ABOUT</a></li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">TRUCKS <span class="caret"></span></a>
				<ul class="dropdown-menu" role="menu">
					<li><a href="<?php echo base_url();?>britatoes">BRITATOES</a></li>
				</ul>
			</li>
			<li><a href="#contact">CONTACT</a></li>
        	<li><a href="#career">CAREER</a></li>
        </ul>
      </div>

      <div class="row" id="home" >
		<div class="col-md-12 text-center">
			<div class="home">
				<img src="<?php echo base_url();?>assets/img/logo-foodtruckindonesia.png">
			</div>
		</div>
	  </div>
	</div>
	</div>
	<div class="container-fluid" style="background-color: #BE1D2D;" id="about">
		<div class="container" style="padding: 40px; color:#fff;">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center">
					<br><br><h1>ABOUT US</h1><br>
					
					<h3>Starting from dreams Director of Food Truck Mandiri Prakarsa, something unique and distinctive when she visited in the United Kingdom (UK)<br><br>
					See great opportunities in Indonesia, she has interesting ideas to make the restaurant business in the form of a mobile</h3>
					<br><br><br>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid" style="background-color: #333;background-image: url('<?php echo base_url();?>assets/img/map-image.png');" id="contact">
		<div class="container" style="padding:40px 0; color:#555;">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center">
					<br><br>
					<h1 style="color:#fff;">CONTACT US</h1>
					<br>
					<form role="form" action="<?php echo base_url();?>submitContact/" method="post">
					  <div class="row">
						  <div class="col-md-6">
							<div class="form-group">
								<label for="txtname"></label>
								<input type="text" class="form-control" name="txtname" id="txtname" placeholder="Enter Name">
							  </div>
						  </div>
						  <div class="col-md-6">
							<div class="form-group">
								<label for="txtemail"></label>
								<input type="email" class="form-control" name="txtemail" id="txtemail" placeholder="Enter email">
							  </div>
						  </div>
					  </div>
					 <div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="txtmessage"></label>
								<input type="text" class="form-control" name="txtmessage" id="txtmessage" placeholder="Enter Message">
							  </div>
						</div>
					 </div>
					 <br>
					  <button type="submit" class="btn btn-danger" style="width:100%; padding:10px 0;">SEND</button>
					</form>
					<br><br><br>
					<h1 style="color:#fff;">OUR GROUP</h1><br>
					<a href="http://lafemme88fm.com/" target="_blank"><img src="<?php echo base_url();?>assets/img/logolafemme88fm.png" width="120"></a>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<a href="http://rightkliq.com/" target="_blank"><img src="<?php echo base_url();?>assets/img/logorightkliq.png" width="80"></a>
					<br><br><br><br>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container-fluid" style="background-color: #BE1D2D;" id="career">
		<div class="row">
			<div class="col-md-4" style="background-color: #cf2031;">
				<div class="body-title">
					<h2>
						JOIN US
					</h2>
					<br>
					<p>
						Want to be part of our mobile kitchen team?
						<br>
						By joining us, we guarantee that you'll have a whole lot different of experiences in pursuing your career in culinary service!
						<br><br>
						In Food Truck Indonesia, we believe that our success will be gained from our people. Not only for them who can cook, but also for the creative, active, passionate, and able to work in a team. These are the qualifications that we're looking for! So if you believe that you have what it takes to be part of a team to create history in Indonesia's food truck industry, JOIN US NOW!
					</p>
					<br><br>
				</div>
			</div>
			<div class="col-md-8" style="background-color: #BE1D2D;">
				<div class="body-title">
					<h2>
						&nbsp;&nbsp;How to Apply
					</h2>
					<br>
					<p>
						&nbsp;&nbsp;&nbsp;Send us your CV to career@foodtruckindonesia.com or fill the form below to apply!
					</p>
					<div class="col-md-6">
						<form role="form" action="<?php echo base_url();?>submitCareer/" method="post" enctype="multipart/form-data" >
						  	<div class="form-group">
								<label for="txtfullname">Full Name *</label>
								<input type="text" class="form-control" name="txtfullname" id="txtfullname" placeholder="">
						  	</div>
						  	<div class="form-group">
								<label for="txtaddress">Address *</label>
								<input type="text" class="form-control" name="txtaddress" id="txtaddress" placeholder="">
						  	</div>
					  		
							<div class="form-group">
							    <label for="txtphoto">Upload Photo (Optional) </label>
							    <input type="file" name="txtphoto" id="txtphoto">
							</div>
					</div>	
					<div class="col-md-6">
							<div class="form-group">
								<label for="txtmobile">Mobile *</label>
								<input type="text" class="form-control" name="txtmobile" id="txtmobile" placeholder="">
						  	</div>
						  	<div class="form-group">
								<label for="txtemail">Email *</label>
								<input type="text" class="form-control" name="txtemail" id="txtemail" placeholder="">
						  	</div>
						  	<div class="form-group">
							    <label for="txtcv">Upload CV * </label>
							    <input type="file" name="txtcv" id="txtcv">
							</div>
						  	
					</div>	
				
				<div class="row">
					<div class="col-md-6">
						<br>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<button type="submit" class="btn btn-danger">SUBMIT</button>

						</form>
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>
	
    <!-- Site footer -->
    <div class="container-fluid" style="background-color: #FFFFFF;">
		<div class="container">
			<div class="footer text-center">
				<p style="color:#BE1D2D;">&copy; Food Truck Indonesia 2014</p>
			</div>
		</div>
	</div>
  </body>
</html>
