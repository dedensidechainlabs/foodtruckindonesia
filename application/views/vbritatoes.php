<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo (isset($description)?$description:"");?>">
    <meta name="author" content="<?php echo (isset($author)?$author:""); ?>">
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/image/logo.png">
    <title><?php echo (isset($title)?$title:"");?></title>
	
	<link href='http://fonts.googleapis.com/css?family=Cabin+Condensed' rel='stylesheet' type='text/css'>
	<link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/jquery.bxslider.css" rel="stylesheet" />
	<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
	<script src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.bxslider.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.elevatezoom.js"></script>
	<script src="<?php echo base_url();?>assets/js/pinterest_grid.js"></script>
	<script>
		$(function() {
		  $('a[href*=#]:not([href=#])').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			  var target = $(this.hash);
			  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			  if (target.length) {
				$('html,body').animate({
				  scrollTop: target.offset().top
				}, 1000);
				return false;
			  }
			}
		  });
		});
		$(document).ready(function(){
		  $('.bxslider').bxSlider();
		});
		
	</script>
	<script>
		var newURL = window.location.protocol + "//" + window.location.host + window.location.pathname;
		if(newURL != '<?php echo base_url();?>britatoes'){
			$(document).ready(function () {
				target_offset = $('#gallery').offset(),
				target_top = target_offset.top;
				$('html, body').animate({
					scrollTop: target_top
				}, 800);
			});
		}
	</script>
  </head>

  <body>
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	  <div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
		  <a class="navbar-brand" href="#home">BRITATOES</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		  <ul class="nav navbar-nav">
			<li class="active"><a href="<?php echo base_url();?>">FOOD TRUCK INDONESIA</a></li>
		  </ul>
		  
		  <ul class="nav navbar-nav navbar-right">
			<li><a href="#about">ABOUT</a></li>
			<li><a href="#menu">MENU</a></li>
			<li><a href="#findus">FIND US</a></li>
			<li><a href="#gallery">GALLERY</a></li>
			<li><a href="#promo">PROMO OF THE MONTH</a></li>
		  </ul>
		</div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	
	<div class="container-fluid" id="home">
		<div class="row">
			<div class="col-md-6 col-md-offset-3 text-center">
				<div class="imghomebg">
					<img src="<?php echo base_url();?>assets/img/logo-britatoes.png" width="70%">
				</div>
			</div>
		</div>
	</div>
	
	
	<div id="about"></div>
	<div class="container-fluid" style="background-color: #BE1D2D;">
		<div class="row">
			<div class="col-md-5 col-md-offset-1 text-center">
				<br><br><br><br><br><br><br>
				<img src="<?php echo base_url();?>assets/img/bg-britatoes.jpg" width="100%">
				<br><br><br><br><br><br>
			</div>
			<div class="col-md-5">
				<br><br><br><br><br>
				<div class="box">
					<div class="boxtitle">
						<h1>ABOUT BRITATOES</h1>
					</div>
					<div class="boxdesc">
						<p>Enjoying delicious meals is a special moment for almost everyone. And taste, is the most important thing that make it so.

BRITATOES. A special meal serving potatoes along with loads of flavor, cooked in different techniques, giving special moments to everyone who's enjoying it.

Premium quality potatoes combined with signature savor, perfect to be enjoyed in any moments with family and friends.

Various toppings will spoil the lovers to be creative while enjoying the luring tasty meals.

Not only these, all of the experiences can be gained in different ambiences as it will be served in a foodtruck which designed particularly to present British quality potatoes. But the taste, will suit your tongue perfectly.</p>
					</div>
					<br>
				</div>
			</div>
		</div>
	</div>
	
	<div  id="menu"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="boxtitle">
					<br><br><br><br><h1><span>OUR MENU</span></h1><br><br>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 text-center">
				<div class="boxtitle">
					<!--<h3><span>WESTERN MENU</span></h3><br>-->
					<img src="<?php echo base_url();?>assets/img/menu/western-menu.png" width="100%"><br><br>
				</div>
				<ul class="bxslider">
					<li><img src="<?php echo  base_url();?>assets/img/menu/Menu Western_Baked Chicken Salad.png"  alt="Britatoes - Menu Western_Baked Chicken Salad" /></li>
					<li><img src="<?php echo  base_url();?>assets/img/menu/Menu Western_Baked Chicken Tikka Masala.png"  alt="Britatoes - Menu Western_Baked Chicken Tikka Masala" /></li>
					<li><img src="<?php echo  base_url();?>assets/img/menu/Menu Western_Baked Chicken Tikka Masala.png"  alt="Britatoes - Menu Western_Baked Chicken Tikka Masala" /></li>
					<li><img src="<?php echo  base_url();?>assets/img/menu/Menu Western_Baked Creamy Mushroom.png"  alt="Britatoes - Menu Western_Baked Creamy Mushroom" /></li>
					<li><img src="<?php echo  base_url();?>assets/img/menu/Menu Western_Baked Smoked Beef & Hunter Style.png"  alt="Britatoes - Menu Western_Baked Smoked Beef & Hunter Style" /></li>
					<li><img src="<?php echo  base_url();?>assets/img/menu/Menu Western_Baked Tuna Salad.png"  alt="Britatoes - Menu Western_Baked Tuna Salad" /></li>
					<li><img src="<?php echo  base_url();?>assets/img/menu/Menu Western_Chips Beef Bolognese.png"  alt="Britatoes - Menu Western_Chips Beef Bolognese" /></li>
					<li><img src="<?php echo  base_url();?>assets/img/menu/Menu Western_French Fries Smoked Beef & Hunter Style.png"  alt="Britatoes - Menu Western_French Fries Smoked Beef & Hunter Style" /></li>
					<li><img src="<?php echo  base_url();?>assets/img/menu/Menu Western_Wedges Cheddar Cheese.png"  alt="Britatoes - Menu Western_Wedges Cheddar Cheese" /></li>
					<li><img src="<?php echo  base_url();?>assets/img/menu/Menu Western_Wedges Fruit Punch.png"  alt="Britatoes - Menu Western_Wedges Fruit Punch" /></li>
				</ul>
				<br><br><br>
			</div>
			<div class="col-md-4 text-center">
				<div class="boxtitle">
					<!--<h3><span>ASIA MENU</span></h3><br>-->
					<img src="<?php echo base_url();?>assets/img/menu/our-signature.png" width="100%"><br><br>
				</div>
				<ul class="bxslider">
					<li><img src="<?php echo  base_url();?>assets/img/menu/Our Signature_Banger & Mash.png" alt="Britatoes - Our Signature_Banger & Mash"/></li>
					<li><img src="<?php echo  base_url();?>assets/img/menu/Our Signature_Fish & Chips.png" alt="Britatoes - Our Signature_Fish & Chips"/></li>
				</ul>
				<br><br><br>
			</div>
			<div class="col-md-4 text-center">
				<div class="boxtitle">
					<!--<h3><span>KIDS MENU</span></h3><br>-->
					<img src="<?php echo base_url();?>assets/img/menu/kids-menu.png" width="100%"><br><br>
				</div>
				<ul class="bxslider">
					<li><img id="zoom_05" src="<?php echo  base_url();?>assets/img/menu/kids-comingsoon.png"  data-zoom-image="<?php echo  base_url();?>assets/img/menu/kids-comingsoon.png" /></li>
				</ul>
				<br><br><br>
			</div>
		</div>
	</div>
	<div id="findus"></div>
	<br><br><br><br><br><br>
	
	<div class="container-fluid" style="background-color: #BE1D2D;">
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="boxtitle">
					<br><br><br><br><h1>FIND US</h1><br><br>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-5">
				<div class="boxtitles">
					<img id="zoom_05" src="<?php echo  base_url();?>assets/img/find-me.png"/ width="100%">
				</div>
			</div>
			<div class="col-md-3">
				<a class="twitter-timeline"  href="https://twitter.com/Britatoes" data-widget-id="552188537471107072">Tweets by @Britatoes</a>
				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
          
			</div>
			<div class="col-md-3">
				<div style="background:#333;">
				<div id="fb-root"></div>
				<script>(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=490590447735463&version=v2.0";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));</script>
				<div class="fb-like-box" data-href="https://www.facebook.com/britatoes" data-height="500" data-colorscheme="dark" data-show-faces="false" data-header="true" data-stream="true" data-show-border="true"></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="footer col-md-6 col-md-offset-3 text-center">
				<a href="http://facebook.com/britatoes" target="_blank"><img src="<?php echo base_url();?>assets/img/icon-fb.png" width="40"></a> &nbsp;&nbsp;&nbsp;&nbsp;
				<a href="http://twitter.com/britatoes" target="_blank"><img src="<?php echo base_url();?>assets/img/icon-twitter.png" width="40"></a> &nbsp;&nbsp;&nbsp;&nbsp;
				<a href="http://instagram.com/britatoes" target="_blank"><img src="<?php echo base_url();?>assets/img/icon-instagram.png" width="40"></a>
				<br><br>
			</div>
			<br>
		</div>
	</div>
	
	<br><br><br><br><br><br><br>
	
	<div id="gallery">
	<br><br><br><br>
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<h1>OUR GALLERY</h1>
				<br><br>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 text-center">
				<section id="galleries">
					<?php foreach($qgallery as $row): ?>
					<article class="white-panel"> <img src="<?php echo base_url();?>assets/img/gallery/<?php echo $row->GALLERYPATH;?>"> </article>
					<?php endforeach; ?>
				</section>
				<br><br>
				
						<?php echo $links; ?>
						
			</div>
		</div>
	</div>
	<br><br><br><br><br>
	
	<div  id="promo"></div>
	<div class="container-fluid" style="background-color: #BE1D2D;">
		<div class="row">
			<div class="col-md-4 col-md-offset-4 text-center">
				<br><br><br><br>
				<h1 style="color:#fff;">PROMO OF THE MONTH</h1><br>
				<img class="img-circle" id="zoom_05" src="<?php echo  base_url();?>assets/img/promo-1.png"/ width="100%">
				<br><br><br><br><br>
			</div>
		</div>
	</div>
	
	<div class="container-fluid" >
		<div class="container">
			<div class="footer col-md-12 text-center">
				<h4>&copy; Food Truck Indonesia 2014</h4>
			</div>
		</div>
	</div>
	<script>
	$(document).ready(function() {
		$('#galleries').pinterest_grid({
			no_columns: 5,
			padding_x: 10,
			padding_y: 10,
			margin_bottom: 50,
			single_column_breakpoint: 700
		});
	});
	</script>
	<script>
		$("#zoom_05a").elevateZoom({ zoomType : "inner", cursor: "crosshair" });
	</script>
  </body>
</html>
