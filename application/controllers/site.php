<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends CI_Controller 
{
	public function __construct()
    {
        parent::__construct();
		$this->load->helper("url");
		$this->load->helper("form");
		$this->load->model("mcontact");
		$this->load->model("mcareer");
		$this->load->model("mgallery");
		$this->load->library("pagination");
    }
	
	public function index()
	{
		$data['title'] = "Food Truck Indonesia";
		$data['description'] = "Food Truck Indonesia";
		$data['author'] = "Food Truck Indonesia";
		$this->load->view('vindex',$data);
	}
	
	function submitContact()
	{
		$data['title'] = "Food Truck Indonesia";
		$data['description'] = "Food Truck Indonesia";
		$data['author'] = "Food Truck Indonesia";

		$txtname = $this->input->post("txtname");
		$txtemail = $this->input->post("txtemail");
		$txtmessage = $this->input->post("txtmessage");
		
		$this->load->helper('date');
		$ipaddress = $this->input->ip_address();
		$datenow = date("Y-m-d");
		$timenow = date('H:i:s');
		
		$data['ipaddress'] = $ipaddress;
		$data['datenow'] = $datenow;
		$data['timenow'] = $timenow;
	
		$contact = array('CONTACTNAME' => $txtname,
						'CONTACTEMAIL' => $txtemail,
						'CONTACTMESSAGE' => $txtmessage,
						'CONTACTIP' => $ipaddress,
						'CONTACTDATE' => $datenow,
						'CONTACTTIME' => $timenow
						);
		$this->mcontact->submitContact($contact);

		$to = 'contact@foodtruckindonesia.com';
		$subject = "Food Truck Indonesia | Contact";

		$message = "
		<html>
		<head>
		<title>Food Truck Indonesia | Contact</title>
		</head>
		<body>
		<table width='70%'>
			<tr>
				<td><img src='http://foodtruckindonesia.com/assets/img/logo-foodtruckindonesia.png' width='30%'></td>
			</tr>
			<tr>
				<td>
				<p>
					You have a message from:<br><br>
					Name : $txtname<br>
					Email : $txtemail<br>
					Message : $txtmessage<br><br>
				</p>
				</td>
			</tr>
		</table>
		</body>
		</html>
		";

		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		// More headers
		$headers .= 'From: contact@foodtruckindonesia.com' . "\r\n";
	
		mail($to,$subject,$message,$headers);
		//END send mail notification
		echo "<script>alert('Thanks for Contact Us!');</script>";
		echo "<script>window.history.back();</script>";
	}

	function submitCareer()
	{
		$data['title'] = "Food Truck Indonesia";
		$data['description'] = "Food Truck Indonesia";
		$data['author'] = "Food Truck Indonesia";

		$txtfullname = $this->input->post("txtfullname");
		$txtaddress = $this->input->post("txtaddress");
		$txtmobile = $this->input->post("txtmobile");
		$txtemail = $this->input->post("txtemail");
		$txtphoto = $this->input->post("txtphoto");
		$txtcv = $this->input->post("txtcv");

		$ipaddress = $this->input->ip_address();
		$datenow = date("Y-m-d");
		$timenow = date('H:i:s');
		
		$data['ipaddress'] = $ipaddress;
		$data['datenow'] = $datenow;
		$data['timenow'] = $timenow;

		$config['upload_path'] = './file/photo/';
		$config['allowed_types'] = 'jpg|png';
		$config['max_size'] = '2000';
		$config['max_width'] = '1024';
		$config['max_height'] = '768';
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		$field_name_photo = "txtphoto";
		if(!$this->upload->do_upload($field_name_photo)){
			echo $this->upload->display_errors();
		}else{
			//echo 'sukses photo';
		}
		
		$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
		$file_name_photo = $upload_data['file_name'];

		$config['upload_path'] = './file/cv/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|doc|xml|docx';
		$config['max_size'] = '2000';
		$config['max_width'] = '1024';
		$config['max_height'] = '768';
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		$field_name_cv = "txtcv";
		if(!$this->upload->do_upload($field_name_cv)){
			echo $this->upload->display_errors();
		}else{
			//echo 'sukses cv';
		}
		$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
		$file_name_cv = $upload_data['file_name'];

		$pathphoto = base_url().'file/photo/'.$file_name_photo;
		$pathcv = base_url().'file/cv/'.$file_name_cv;
		
		$career = array('CAREERFULLNAME' => $txtfullname,
						'CAREERADDRESS' => $txtaddress,
						'CAREERMOBILE' => $txtmobile,
						'CAREEREMAIL' => $txtemail,
						'CAREERCV' => $pathcv,
						'CAREERPHOTO' => $pathphoto,
						'CAREERIP' => $ipaddress,
						'CAREERDATE' => $datenow,
						'CAREERTIME' => $timenow
						);
		$this->mcareer->submitCareer($career);


		$to = 'career@foodtruckindonesia.com';
		$subject = "Food Truck Indonesia | Career";

		$message = "
		<html>
		<head>
		<title>Food Truck Indonesia | Career</title>
		</head>
		<body>
		<table width='70%'>
			<tr>
				<td colspan='2' style='background-color: #cf2031; text-align:center;'><img src='http://foodtruckindonesia.com/assets/img/logo-foodtruckindonesia.png' width='30%'></td>
			</tr>
			<tr>
				<td colspan='2' style='text-align:center;'><h3>You have a message for submit career from:</h3></td>
			</tr>
			<tr>
				<td><h5>Photo</h5></td>
				<td><img src='http://foodtruckindonesia.com/file/photo/$file_name_photo' width='100'></td>
			</tr>
			<tr>
				<td><h5>Fullname</h5></td>
				<td><h5>: $txtfullname</h5></td>
			</tr>
			<tr>
				<td><h5>Address</h5></td>
				<td><h5>: $txtaddress</h5></td>
			</tr>
			<tr>
				<td><h5>Mobile</h5></td>
				<td><h5>: $txtmobile</h5></td>
			</tr>
			<tr>
				<td><h5>Email</h5></td>
				<td><h5>: $txtemail</h5></td>
			</tr>
			<tr>
				<td><h5>CV</h5></td>
				<td><h5>: <a href='$pathcv' target='_blank'>here</a></h5></td>
			</tr>
			<tr>
				<td colspan='2' style='background-color: #cf2031; color: #fff; text-align:center;'><h5>Food Truck Indonesia</h5></td>
			</tr>
		</table>
		</body>
		</html>
		";

		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		// More headers
		$headers .= 'From: career@foodtruckindonesia.com' . "\r\n";
	
		mail($to,$subject,$message,$headers);
		//END send mail notification
		echo "<script>alert('Thanks for Join With Us!');</script>";
		echo "<script>window.history.back();</script>";
	}

	function do_upload(){
		$config['upload_path'] = './file/photo/';
		$config['allowed_types'] = 'jpg|png';
		$config['max_size'] = '2000';
		$config['max_width'] = '1024';
		$config['max_height'] = '768';
		$this->load->library('upload', $config);

		// Alternately you can set preferences by calling the initialize function. Useful if you auto-load the class:
		$this->upload->initialize($config);
		$field_name = "txtphoto";
		if(!$this->upload->do_upload($field_name)){
			echo $this->upload->display_errors();
		}else{
			echo 'sukses';
		}
	}

	function britatoes()
	{
		$data['title'] = "Food Truck Indonesia";
		$data['description'] = "Food Truck Indonesia";
		$data['author'] = "Food Truck Indonesia";
		
		$config = array();
        $config["base_url"] = base_url() . "britatoes/";
        $config["total_rows"] = $this->mgallery->record_count();
        $config["per_page"] = 10;
        $config["uri_segment"] = 2;
		
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		
        $this->pagination->initialize($config);
 
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $data["qgallery"] = $this->mgallery->fetch_tgallery($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
		
		$this->load->view('vbritatoes',$data);
	}
	
}